package com.fiftechfabo.challenge.challenge.controllers;

import com.fiftechfabo.challenge.challenge.models.WSResponse;
import com.fiftechfabo.challenge.challenge.services.CSVUtils;
import com.fiftechfabo.challenge.challenge.services.SftpConnection;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;


@RestController
public class EmployeesController {
    @GetMapping("/")
    public String hola() throws IOException {
        RestTemplate restTemplate = new RestTemplate();
        WSResponse e = restTemplate.getForObject("http://dummy.restapiexample.com/api/v1/employees", WSResponse.class);
        CSVUtils csv = new CSVUtils(e.getEmployees());
        String fileName = "employees-" + System.currentTimeMillis()+".csv";
        csv.createCSV(fileName);
        SftpConnection con = new SftpConnection();
        con.upload(fileName);


        return "test";
    }
    //sftp-server
    //sftp_user
//latterchangetocertificate
}
