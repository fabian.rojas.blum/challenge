package com.fiftechfabo.challenge.challenge.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WSResponse {
    private String status;
    private Employee[] data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Employee[] getData() {
        return data;
    }

    public void setData(Employee[] data) {
        this.data = data;
    }

    public List<String[]> getEmployees() {
        List<String[]> employees = new ArrayList<String[]>();
        employees.add(Employee.HEADER);
        for (Employee e:data){
            employees.add(e.getEmployee());
        }
        return employees;
    }
}
