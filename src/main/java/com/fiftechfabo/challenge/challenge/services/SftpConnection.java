package com.fiftechfabo.challenge.challenge.services;

import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;
import org.springframework.integration.sftp.session.SftpSession;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;

public class SftpConnection {

    private DefaultSftpSessionFactory gimmeFactory(){
        DefaultSftpSessionFactory factory = new DefaultSftpSessionFactory();
        factory.setHost("35.239.166.66");
        factory.setPort(22);
        factory.setAllowUnknownKeys(true);
        factory.setUser("sftp_user");
        factory.setPassword("latterchangetocertificate");
        return factory;
    }

    public void upload(String name){

        SftpSession session = gimmeFactory().getSession();
        InputStream resourceAsStream =
                SftpConnection.class.getClassLoader().getResourceAsStream(name);
        try {
            session.write(resourceAsStream, "/home/sftp_user/csvs/" + name);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        session.close();
    }

    public String download(){
        SftpSession session = gimmeFactory().getSession();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            session.read("upload/downloadme.txt", outputStream);
            return new String(outputStream.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }


}